class Reader {
  String fio;
  String faculty;
  String dateOfBith;
  int phoneNum;
  Reader(
      {required this.fio,
      required this.faculty,
      required this.dateOfBith,
      required this.phoneNum});
  void printAll_() {
    print(
        'Студент:\nФ.И.О: $fio\nФакультет: $faculty\nДата рождения: $dateOfBith\nНомер телефона: $phoneNum\n');
  }

  void takeBook(String name, int book) {
    book.toString;
    print('$name: взял $book книги ');
  }

  void returnBook(String name, int book) {
    print('\n$name: вернул $book книги ');
  }
}

class Library {
  String nameBook;
  String author;
  Library({required this.nameBook, required this.author});
  void takeLibrary(String name) {
    print('$name Взял следующие книги: $nameBook, Автор книги: $author');
  }

  void takeBook() {
    print('Книга: $nameBook, Автор: $author');
  }

  void returnLibrary(String name) {
    print('$name Вернул следующие книги: $nameBook, Автор книги: $author');
  }

  void returnBook() {
    print('Книга: $nameBook, Автор: $author');
  }
}

void main(List<String> args) {
  Reader reader = Reader(
      fio: 'Okean Mamatov',
      faculty: 'Energetik',
      dateOfBith: '09.12.1996',
      phoneNum: 0551151607);

  Library book = Library(nameBook: 'Мертвые души', author: 'Николай Гоголь');
  Library book2 = Library(nameBook: 'Пиковая дама', author: 'Александр Пушкин');
  Library book3 = Library(nameBook: 'Война и мир', author: 'Лев Толстой');

  reader.printAll_();
  reader.takeBook(reader.fio, 3);
  book.takeLibrary(reader.fio);
  book2.takeBook();
  book3.takeBook();
  reader.returnBook(reader.fio, 2);
  book.returnLibrary(reader.fio);
  book2.returnBook();
}
